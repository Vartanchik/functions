const getSum = (str1, str2) => {
  if (typeof str1 !== "string" || typeof str2 !== "string" || isNaN(str1) || isNaN(str2))
    return false;
  
  let num1 = str1.length !== 0 ? parseInt(str1, 10) : 0;
  let num2 = str2.length !== 0 ? parseInt(str2, 10) : 0;
  return (num1 + num2).toString();
};

const getQuantityPostsByAuthor = (listOfPosts, authorName) => {
  let postsNumber = 0;
  let commentsNumber = 0;

  for (let post of listOfPosts) {
    if (post.author === authorName)
      postsNumber++;
    
    if (post.hasOwnProperty("comments"))
      for (let comment of post.comments)
        if (comment.author === authorName)
          commentsNumber++;
  }
  
  return `Post:${postsNumber},comments:${commentsNumber}`;
};

const tickets = (people) => {
  const ticketCost = 25;
  let wallet = 0;

  for (let personMoney of people) {
    let change = personMoney - ticketCost;

    if (change <= wallet) {
      wallet += personMoney - change;
      continue;
    }

    return "NO";
  }

  return "YES";
};

module.exports = {getSum, getQuantityPostsByAuthor, tickets};
